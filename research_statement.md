### Is QUIC streaming ready? A long look at the practical impact of QUIC for streaming.


##### Abstract:

Studies evaluating the impact of QUIC against TCP in the web stack shows that QUIC outperforms the latter in most cases while this is not the case with media streaming. Recent studies [1] have shown that QUIC, a fully reliable stream-oriented transport protocol results in poor performance for media streaming. However, QUIC being a userspace protocol, it can be easily modified without any kernel dependencies to improve its streaming performance. In this research, we propose to integrate a custom QUIC extension for streaming in VLC. We will compare the Quality of Experience(QoE) of Real-time protocol (RTP) and Dynamic Adaptive Streaming over HTTP (DASH) over vanilla QUIC (draft-22) as well as with a fully custom extension of QUIC which is developed as a part of this research.  Previous studies [2, 3] show that a tweaked QUIC implementation can actually outperform TCP in the context of media streaming. The custom QUIC designed and implemented as part of this study will enhance on previous works. Subsequently, it aims at developing an optimised general-purpose custom QUIC extension consisting of features such as deadline and dependency awareness, Forward Error Correction, selective reliable transport, head-of-line blocking resilience [7] and much more.

##### AIntroduction

QUIC is a new transport layer network protocol that resides over the User Datagram Protocol (UDP) in an end-to-end encrypted format using TLS 1.3. Similar to what TCP offers, QUIC provides in-order reliability, redesigned congestion control, flow control, error handling, etc. By using UDP,  QUIC solves a long-standing problem of TCP called head-of-line blocking. This, in turn, has a large impact on how we can manage higher-level protocols, such as HTTP. According to IETF, one of the officially supported application layer protocols by QUIC is HTTP/3. HTTP/3 is similar to HTTP/2 but can be seen as an adaptation of the latter specifically tailored to be run over QUIC as the transport protocol.

**Head-of-line Blocking**: HOL blocking happens when a request for multiple objects is made, and an object gets stuck because a preceding object got delayed. Generally HOL blocking can happen if segments get lost, subsequent messages have to wait for the successful retransmission in the receiver queue and are thus delayed.

This research is carried out in VLC to study the practical impact of QUIC on media streaming. VLC is a popular media streaming application that is used by millions. VLC allows media streaming using multiple platforms such as RTP, RTSP, HTTP/1.1, UDP, IceCast and MS-WMSP. Initially, this study proposes the QUIC protocol to be exposed over RTP and for DASH through HTTP/3. RTP is used by applications for transmitting real-time data, such as audio, video or simulation data over UDP[6]. Applications using RTP prioritise low latency over reliability.

Although more complicated than DASH, where media is delivered as a series of HTTP GET requests, RTP is much more extensible to let applications customise how data should be formatted and transferred between participants through the use of RTP profiles.

Media streams delivered using HTTP generally use either DASH or HLS. Since QUIC exposes APIs that enables easier integration with HTTP standard applications, it is easier to implement DASH, while it would take more rigorous configuration to integrate a performant RTP over QUIC. DASH generally employs chunks of media content, typically with a duration between 1 and 10 seconds, listed in a manifest file[2]. DASH generally uses TCP as the underlying transport protocol and thus DASH encounters stalling due to head-of-line blocking within TCP as a result. QUIC mitigates this issue by using several multiplexed streams over a single UDP socket, but not entirely. Although head-of-line blocking still occurs in response to loss, the obstruction is confined to a single stream rather than blocking the entire connection[7]. With some adjustments to optimisation algorithms used in the application and QUIC, QUIC could be used to provide higher quality streaming services for unidirectional media content [3].

However, when expecting to see a better result than media streaming over TCP, one might get disappointed as vanilla QUIC might not outperform the TCP+HTTP/2 stack. Hence, with this study,  we look at how to extend QUIC in order to exhibit better performance and a high quality streaming over QUIC which co-evolves with media streaming applications.

This study aims to answer the following questions:

 * Whether limitations of vanilla QUIC (draft-22) on streaming has a large impact in practice?
 * What techniques can be developed to overcome such limitations to adapt a general-purpose QUIC for streaming?

##### Description/Implementation details

The implementation phase, involves the development of a custom QUIC that extends the current draft-22 for better performance, high quality and low latency streaming. This general-purpose custom QUIC would be a design to support any media streaming application and thus evaluated within VLC to study the practical impact of QUIC for media streaming.

###### Handling Mismatched semantics of RTP and QUIC

QUIC provides a fully reliable, stream-based, connection-oriented service. A connectionless protocol like RTP favours timely transport rather than reliable transport over UDP. Considering this, RTP over QUIC is semantically incorrect. Therefore, there is an existing design of RTP over Connection-oriented transport [8], designed for RTP over TCP, which could be reused for RTP transport over QUIC. Hence, the use of such a design has the advantage of simplicity and both RTP packets and RTCP (RTP control sequence) packets could be sent as a single QUIC stream. However, with this approach, we would still suffer from the head of line blocking. In a protocol like RTP, which strictly prefers timely transfer than reliability, it is unacceptable to tolerate higher latency due to HoL. Thus, HOL blocking prevention in this case is only possible when we have multiple concurrent streams to transport the data and the control sequence.
Custom QUIC extensions

Custom QUIC proposed should contain head-of-line blocking resilience[7], support for selective reliable transport, Deadline Awareness, Message-oriented abstraction, Forward error correction and even proper I-P-B frame handling.

#### Custom QUIC extension features

* Selective Reliable Transport

Palmer et al. [2] suggest a design that transports (a) I-frames reliably (b) P and B frames unreliably. Since P and B frames could be easily recovered from the frame loss. Similarly, The idea of marking some QUIC streams as either reliable, partially reliable, or unreliable was proposed in an IETF draft by Tiesel et al [5].
An adaptive streaming protocol like DASH heavily improves from this approach. However, we need to study if a transport protocol for real-time media must prioritise timeliness over reliability and in-order delivery could gain anything better from this. Typically, RTP prefers timeliness over reliability, we strictly need to adhere within the bounds of the latency limits though we have to sacrifice on QoE. RFC 4588 [9] provides a scheme on how the transmission could be taken care of within the context of better performance.

**Handling Stale Frames**: If we encounter a stale I-frame or P-frame, we introduce a read offset that ignore the non-alive frames to minimise the latency.

* Deadline Awareness

 Applications which should respond to user interactions in real-time highly benefit from determining the usefulness of content before (re-)sending is vital for reducing the amount of unnecessary data sent, and therefore reducing latency as much as possible. Similar to the approach in TCP Hollywood[10], Deadline awareness will be designed using timestamps, message sequence numbers, and playback deadlines. The receiver will need to send a receipt of which messages arrived at which time; these will be used as part of a moving average to calculate current round-trip times (RTTs) to predict the usefulness of future media frames, but are not needed to remove packets from the retransmit queue as this is already handled by regular ACKs within QUIC.

* Message-Oriented Abstraction

The byte-stream used within QUIC by default needs to be replaced with a message-oriented abstraction to allow applications to process units of data independently of each other, allowing out-of-order delivery to occur within the transport as explained by Lubashev[4]. In latency bounded media streaming ecosystems, a transport that supports partially reliable streams could bring substantial increment in performance but at the cost of the frame quality. According to the draft by Lubashev[4], It is possible to avoid the need for partially reliable streams by encoding one message per QUIC stream. When a message expires, the sender can reset the stream, causing RST_STREAM frame to be transmitted, unless all data in the stream has already been fully acknowledged. Likewise, the receiver can send STOP_SENDING frame to indicate its disinterest in the message.

The concept of application data units is important to minimise the number of dependencies where possible: individual frames may require another frame to be useful in media playback, but the application should be able to decode them independently of each other. An application should be able to read individual, meaningful messages instead of attempting to parse (potentially incomplete or corrupt) frames from a raw byte-stream. This requirement also involves attempting to reassemble I-frames which must be split across several QUIC packets into a single message. Henceforth, A message-based abstraction will be required in place of a byte-stream to ensure that frames remain independently decodable by the application.

* Forward Error Correction

A recent IETF draft[11] mentions the use of FEC to improve the QUIC performance in real time sessions. This study promises FEC coding to make packet loss recovery insensitive to the round trip time. Our study aims to assess how this will behave in the wild.  Further, we will use this FEC mechanism to recover from the lost P and B frame packets as argued in [2].

* I-P-B frame handling

P-Frames and B-Frames, only contain information about changes within an existing image and can typically be transmitted within a single QUIC packet without fragmentation due to their small size as compared to I-frame. I-frames, are typically much larger because they contain information about an entire image and will need to be split over multiple QUIC packets.

Note, When it comes to high bitrate streaming or larger resolution, single streams of P-B frames could easily exceed the limit of QUIC's single packet size (typically 1500 bytes). Under such circumstances, we check if the specific P-B frame QUIC streams exceeds the limit, then, P and B frames could be split into multiple separate streams.

Here, we split P-B frame when they are larger than the packet since we indent to reduce fragmentation as much as possible, and, consequently, a try to reduce HOLblocking.

Another eminent challenge in frame handling is the cost-effective reassembling of the I-frames. Few of the solutions would be: (1) we can reassemble the I-frames from the QUIC packets in the application. (2) reassemble the packets within the QUIC extension. (3) Introduce a shim-layer between the application and the custom QUIC where the reassembly could be done.

* Head-Of-Line blocking resilience

Few of the above-mentioned techniques help to reduce the wait for streams due to HOL. Together with these techniques, we propose to split the media stream into several conceptual, independent QUIC/HTTP streams (e.g., 1 for I-frames, 1 for B, 1 for P, 1 for control, etc.) if HOL blocking is found. Moreover, we perform a strict round-robin scheduling scheme on those frames in order to introduce concurrent streams and thus reduce the intra-stream HOL Blocking.

Once the custom QUIC prototype is developed consisting of the above concepts, we deploy it in VLC and run a variety of performance tests to assess the QoE of streaming: (1) Comparison of the RTP and DASH over vanilla QUIC draft-22 based streaming stack in VLC. (2) Comparison of the RTP and DASH over the custom QUIC for the streaming stack which we developed as a part of this research.


* Evaluation metrics

A controlled environment for testing is essential to evaluate the effectiveness of the new extensions. Hence a testbed would be set up to evaluate the custom QUIC against IETF QUIC draft-22 with the following parameters: average playback bitrate, average wait time after seeking, rebuffer rate, time taken to start media streams, frame rate, seeking latency, evaluation of the HOL blocking scenario in the prototype design and evaluation on frequent connection changes.

Note: Once the proposal looks good and we plan to move forward, I will add the deliverables section with the proper timeline.

References:

[1] Divyashri Bhat, Amr Rizk, Michael Zink, Not so QUIC: A Performance Study of DASH over QUIC [2] Mirko Palmer, Thorben Kruger, Balakrishnan Chandrasekaran, Anja Feldmann, The QUIC Fix for Optimal Video Streaming\
[3] Colin Perkins, JÃűrg Ott, Real-time Audio-Visual Media Transport over QUIC\
[4] I. Lubashev, Partially Reliable Message Streams for QUIC\
[5] P. Tiesel, M. Palmer, B. Chandrasekaran, A. Feldmann, J. Ott, Considerations for Unreliable Streams in QUIC\
[6]: H. Schulzrinne, S. Casner, R. Frederick , V. Jacobson, RTP: A Transport Protocol for Real-Time Applications\
[7]: Robin Marx, Tom De Decker, Peter Quax and Wim Lamotte, Of the Utmost Importance: Resource Prioritization in HTTP/3 over QUIC\
[8]: J. Lazzaro, Framing Real-time Transport Protocol (RTP) and RTP Control Protocol (RTCP) Packets over ConnectionOriented Transport\
[9]: J. Rey, D. Leon, A. Miyazaki, V. Varsa, R. Hakenberg, RTP Retransmission Payload Format\
[10]: Stephen McQuistin, Colin Perkins, Marwan Fayed, TCP goes to hollywood\
[11]: I. Swett, M-J. Montpetit, V. Roca, Coding for QUIC\


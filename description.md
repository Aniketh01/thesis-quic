# (A) Project title
**This part should be ready by Jun 22, 2019**

## Project Advisor: Swaminathan/Sarath/Lekshmi

## Project Mentor: <Guide name>

## Team:
  1. AM.EN.U4CSE16___    Student 1
  2. AM.EN.U4CSE16___    Student 2
  3. AM.EN.U4CSE16___    Student 3
  4. AM.EN.U4CSE16___    Student 4

---

# (B) Problem
**This part should be ready by Jul 22, 2019**

## Context of your work
  - Field --> Area --> Subarea --> Topic (from general to specific)
  - Define 20-25 technical terms pertaining to your field/area/topic in a table
  ----
  | Terms   | Description |
  -----
   

## Background
  - [Foundation] Three important papers that formed the basis for your problem
  - [Discussion] What problem(s) do they address + pros and cons of their solution?
  - [Motivation] How did the above papers influence your problem definition? 
  > (in about 3 paragraphs)

## Brief problem description
  - Informal description of your problem
  - Formal statement of your problem
  - Additional information pertaining to the problem
  > (in about 300 words, clear and specific)

## Relevance
  1. Who will benefit from your work?
  2. How will they use your work?
  > (in about 100 words, without muddling)

## Novelty
  1. What is unique about your work? Is it the problem or solution approach or any other? Substantiate.
  2. What distinguishes your work from other related work?
  > (in 2 paragraphs)

**Note**: Don't make tall claims without substantiating them

## Scope
  - Justify your problem is neither too big nor too small but of adequate size for a final year project
  > (in one paragraph)

## Expected contribution
  - Comprehensive survey?
  - New algorithm?
  - Improving existing algorithm?
  - Comparative study?
  - Experimental study?
  > (in clear and unambiguous terms)
  
## Data set
  - What is the source of your data set?
  - Justify your data set is reliable
  - Justify your data set is adequate
  - How do you plan to eliminate noise in your data set?

---

# (C) Solution
**This part should be completed by Sep 16, 2019**

## Approach
  - Terms and definitions
  - Discuss your solution approach in complete detail
  - Justify credibility and appropriateness of your solution

## Block Diagram
  - Detailed block diagram with inputs, outputs, modules - stagewise, intermediate inputs and outputs
  - Every block and every arrow should be annotated
  - The block diagram should be in sync with solution approach

## Algorithm(s)
  - Provide the algorithm(s), connect it with solution approach
  - Discuss the correctness and complexity
  - Provide a running example demonstrating the working of the algorithm
  - Discuss working of algorithm for unusual and corner cases

## Implementation
  - [Source code](https://github.com/username/projectname/blob/master/source/....)

  > (point to your github source code repository)

---

# (D) Experiments

**This part should be completed by Oct 31, 2019**

## Evaluation strategy
  - What is your plan of evaluation?
  - Elaborate your test setup
  - What parameters are you going to measure?
  - Justify these parameters are relevant and adequate
  - How do you plan to report your results?
  - Does your evaluation plan include large experiments?
  - Justify your evaluation strategy is sound

## Data set
  - Define the format of your dataset (or test data)
  - Provide a descrition of your fields in your dataset
  - Provide a description of how you plan to use your dataset
  - Justify your data set is sufficient for basing your experiments

## Results
  - What are your results?
  - How will you report your results? (charts/table/...)
  - Justify your results are valid
  - Justify your results are reliable

## Interpretation of results
  - What do your results say?
  - Do your results in line with your project goal?
  - Do your results include false positives/false negatives?

---

# References
  - author names, title of the paper, conference/journal, year
  > (in standard format)



# Weekly status report

---


## Week of July 1

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Started formulating the thesis plans

### Issues/Concerns/Road blocks
  1. None so far.
 
### Action plan to address issues
  <!--1. Figuring out different plans to improve media streaming over QUIC.-->

### Plans for the next week
  1. Continue formulating ideas and discussing with QUIC working groups. 

### Comments from guide (very specific)

---

## Week of Jul 8

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Figuring out different plans to improve media streaming over QUIC and create a research statement.
  2. Reading various research papers on the same.

### Issues/Concerns/Road blocks
  1. Understanding the basics of QUIC and media streaming.

### Action plan to address issues
  1. 

### Plans for the next week
  1. List out various features to be included within QUIC

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 29

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Reading various research papers on the same.
  2. Listed various features in to be included to improve QUIC for streaming.

### Issues/Concerns/Road blocks
  1. Understanding needed features

### Action plan to address issues
  1. Complete the first phase of the research statement and get it reviewed

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

---

## Week of Aug 5

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Send for reviews to team at Hasselt university, and to mentor in Amrita. 
  
### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 12

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Recieved reviews on the research goal and started refactoring the whole idea. 

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. Fix all the issues mentioned and send them for review once again.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 19

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. All the reviews were rectified and send to MPI group for guidance on the same.

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 26

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Started implementing HTTP POST in VLC

### Issues/Concerns/Road blocks
  1. Understanding how the HTTP idempotency work.

### Action plan to address issues
  1. Do HTTP POST as multiple connections.

### Plans for the next week
  1. Get the code written reviewed and merged into VLC codebase.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 2

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Refactored the research statement once again.
  2. Wrote code for QUIC TLS APIs in gnuTLS.
  3. This research idea got accepted for SOSP'19 ACM students research competition.

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. Continue working on the QUIC TLS APIs into gnuTLS.
  
### Comments from guide (very specific)
  1. ...
  2. ...

---

<!--## Week of Sep 9-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Sep 16-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Sep 23-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Sep 30-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Oct 7-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Oct 14-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Oct 21-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

<!--## Week of Oct 28-->

<!--### Phase-->
<!--  - [ ] Team formation-->
<!--  - [ ] Problem definition-->
<!--  - [ ] Implementation-->
<!--  - [ ] Experimentation-->
<!--  - [ ] Report writing-->

<!--### Accomplishments of the week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Issues/Concerns/Road blocks-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Action plan to address issues-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Plans for the next week-->
<!--  1. ...-->
<!--  2. ...-->

<!--### Comments from guide (very specific)-->
<!--  1. ...-->
<!--  2. ...-->

<!------->

